import re
import matplotlib.pyplot as plt

# blobs
file_names = []
for i in range(2, 6):
    for j in range(5):
        file_names.append('out_{}blobs{}.txt'.format(i, j))
# circles
for i in range(5):
    file_names.append('out_circles{}.txt'.format(i))
# moons
for i in range(5):
    file_names.append('out_moons{}.txt'.format(i))

file_names = ['intermediate/out_int{}.txt'.format(i) for i in range(12)]

for file in file_names:
    with open('output/{}'.format(file)) as f:
        lines = f.readlines()
        f.close()

    # read nodes
    nodes = []
    stringNodes = re.findall(r'([+-]?\d+\.\d+)(E-\d+)?', lines[1])
    for i in range(0, len(stringNodes), 2):
        x = float(stringNodes[i][0] + stringNodes[i][1])
        y = float(stringNodes[i + 1][0] + stringNodes[i + 1][1])
        node = [x, y]
        nodes.append(node)

    # read edges
    edges = []
    stringEdges = re.findall(r'\(\d+, \d+\)', lines[2])
    for e in stringEdges:
        node = re.findall(r'\d+', e)
        edges.append([int(node[0]), int(node[1])])

    # plot nodes
    x = [node[0] for node in nodes]
    y = [node[1] for node in nodes]
    plt.figure()
    plt.scatter(x, y, s=10, c='b')

    # plot edges
    for e in edges:
        plt.plot([nodes[e[0]][0], nodes[e[1]][0]], [nodes[e[0]][1], nodes[e[1]][1]], c='b')
    plt.title(file)
    plt.show()
    plt.close()

