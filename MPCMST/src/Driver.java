import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Driver {

    public static ArrayList<Node> genNodes(int n, int idStart) {
        // generate random set of nodes
        Random rnd = new Random();
        ArrayList<Node> nodes = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            nodes.add(new Node(i + n * idStart, rnd.nextInt(n) + n * idStart, rnd.nextInt(n) + n * idStart));
        }
        return nodes;
    }

    public static void write(Graph g, String fileName, double duration) {
        // write to python graph visualizer location
        try {
            FileWriter myWriter = new FileWriter("output\\" + fileName + ".txt");
            myWriter.write(g.toString());
            myWriter.write("Duration (s): " + duration);
            myWriter.close();
            System.out.println("Successfully wrote to file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static Graph read(String fileName) {
        Pattern number = Pattern.compile("[-+]?\\d*\\.\\d+|\\d+");
        Matcher m;
        int n = 0;
        int k = 1;
        int nodeCount = 0;
        HashMap<Integer, Node> nodes = new HashMap<>();
        try {
            File myObj = new File("input\\" + fileName + ".txt");
            Scanner myReader = new Scanner(myObj);
            boolean firstLine = true;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                m = number.matcher(data);
                ArrayList<String> args = new ArrayList<>();
                while (m.find()) {
                    args.add(m.group(0));
                }
                if (firstLine) {
                    n = Integer.parseInt(args.get(0));
                    k = Integer.parseInt(args.get(1));
                    firstLine = false;
                } else {
                    nodes.put(nodeCount, new Node(nodeCount, Double.parseDouble(args.get(0)), Double.parseDouble(args.get(1))));
                    nodeCount++;
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        Graph g = new Graph(nodes);
        g.fullyConnect();
        return g;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static void testing(int n, int machines, double eps, int k, boolean showIntermediates) {
        // make graph
        double start = System.currentTimeMillis();
        Graph G = new Graph();
        G.addNodes(genNodes(n, 0));
        G.fullyConnect();
        System.out.println("Graph building: " + (System.currentTimeMillis() - start) / 1000);

        // build mst, simulate parallel
        start = System.currentTimeMillis();
        System.out.println("Building MST");
        Graph mst = MPC.parallelMST(G, machines, eps, showIntermediates);
        System.out.println("Building cluster");
        Graph cluster = Graph.combineGraphs(MSTBuilder.cluster(mst, k));
        double duration = (System.currentTimeMillis() - start) / 1000;
        System.out.println("Duration: " + duration);
        write(mst, "out_test" + n, duration);
    }

    public static void computeFromInput(int machines, double eps, boolean showIntermediates) {
        // generate msts from files
        // store filenames
        ArrayList<String> fileNames = new ArrayList<>();
        // add blobs
        for (int i = 2; i <= 5; i++) {
            for (int j = 0; j <= 4; j++) {
                fileNames.add(i +  "blobs" + j);
            }
        }
        // add circles
        for (int i = 0; i <= 4; i++) {
            fileNames.add("circles" + i);
        }
        // add moons
        for (int i = 0; i <= 4; i++) {
            fileNames.add("moons" + i);
        }

        // build graphs
        for (String file : fileNames) {
            // read file
            System.out.println("Computing " + file);
            Graph in = read(file);

            // timer
            double start = System.currentTimeMillis();

            // compute mst
            Graph mst = MPC.parallelMST(in, machines, eps, showIntermediates);

            // compute clustering
            int k = 2;
            if (isNumeric(file.substring(0, 1))) {
                k = Integer.parseInt(file.substring(0, 1));
            }
            Graph cluster = Graph.combineGraphs(MSTBuilder.cluster(mst, k));

            // check duration of build
            double duration = (System.currentTimeMillis() - start) / 1000;
            System.out.println("Duration: " + duration);

            // write to file
            write(cluster, "out_" + file, duration);
        }
    }

    public static void main(String[] args) {
//        // low eps to really see change in intermediate msts
//        testing(250, 10, .2, 1, true);
//        testing(1000, 10, .497, 1, false);
//        testing(2000, 10, .497, 1, false);

        // actual data
        computeFromInput(10, 0.497, false);
    }
}
