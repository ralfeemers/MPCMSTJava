public class Edge {
    int u;
    int v;
    double w;

    public Edge(int u, int v, double w) {
        this.u = u;
        this.v = v;
        this.w = w;
    }

    @Override
    public String toString() {
        return "(" + u + ", " + v + ")";    // graph drawing output
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Edge)) {
            return false;
        }

        Edge e = (Edge) o;

        return (e.u == this.u && e.v == this.v)
                || (e.u == this.v && e.v == this.u);
    }
}
