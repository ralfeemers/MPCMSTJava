import java.util.ArrayList;
import java.util.HashMap;

public class Graph {
    HashMap<Integer, Node> nodes;
    ArrayList<Edge> edges;

    public Graph() {
        nodes = new HashMap<>();
        edges = new ArrayList<>();
    }

    public Graph(HashMap<Integer, Node> initNodes) {
        this.nodes = new HashMap<>();
        for (Node n : initNodes.values()) {
            nodes.put(n.id, n.copy());
        }
        edges = new ArrayList<>();
    }

    public static double dist(Node u, Node v) {
        return Math.sqrt(Math.pow(u.x - v.x, 2) + Math.pow(u.y - v.y, 2));
    }

    // combine multiple graphs into single graph
    public static Graph combineGraphs(ArrayList<Graph> graphs) {
        Graph combined = new Graph();
        for (Graph g : graphs) {
            combined.edges.addAll(g.edges);
            combined.addNodes(new ArrayList<>(g.nodes.values()));
        }
        return combined;
    }

    // make graph fully connected
    public void fullyConnect() {
        edges.clear();
        for (Node u : nodes.values()) {
            for (Node v : nodes.values()) {
                if (!u.equals(v)) {
                    u.addNode(v);
                    Edge e = new Edge(u.id, v.id, dist(u, v));
                    addEdge(e);
                }
            }
        }
    }

    // calculate edges based on adjacency
    public void calcEdges() {
        edges.clear();
        for (Node u : nodes.values()) {
            for (Node v : u.adjacency.values()) {
                if (!u.equals(v)) {
                    Edge e = new Edge(u.id, v.id, dist(u, v));
                    addEdge(e);
                }
            }
        }
    }

    // set nodes
    public void setNodes(HashMap<Integer, Node> nodes) {
        this.nodes = nodes;
    }

    // add node
    public void addNode(Node n) {
        if (!nodes.containsKey(n.id)) {
            nodes.put(n.id, n);
        }
    }

    // add multiple nodes
    public void addNodes(ArrayList<Node> nodes) {
        for (Node n : nodes) {
            addNode(n);
        }
    }

    // set edges
    public void setEdges(ArrayList<Edge> edges) {
        this.edges = edges;
    }

    // add edge
    public void addEdge(Edge e) {
        if (!edges.contains(e) && e.u != e.v) {
            edges.add(e);
        }
    }

    public Graph copy() {
        return MPC.partition(this, 1).get(0);
    }

    @Override
    public String toString() {
        return "\nNodes: " + nodes.values().toString() + "\nEdges: " + edges.toString() + "\n";
    }
}
