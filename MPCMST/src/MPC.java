import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.*;

public class MPC {
    // build partitioning of a graph, also works as copy of graph with single machine
    public static ArrayList<Graph> partition(Graph graph, int machines) {
        // init partitioning
        ArrayList<Graph> partitioning = new ArrayList<>();
        for (int i = 0; i < machines; i++) {
            partitioning.add(new Graph());
        }

        // partition edges
        Random rnd = new Random();
        for (Edge e : graph.edges) {
            // select machine
            Graph part = partitioning.get(rnd.nextInt(machines));

            // add edge
            part.addEdge(e);

            // handle node u
            Node u;
            if (!part.nodes.containsKey(e.u)) {
                u = graph.nodes.get(e.u).copy();
                part.addNode(u);
            } else {
                u = part.nodes.get(e.u);
            }

            // handle node v
            Node v;
            if (!part.nodes.containsKey(e.v)) {
                v = graph.nodes.get(e.v).copy();
                part.addNode(v);
            } else {
                v = part.nodes.get(e.v);
            }

            // make nodes adjacent
            u.addNode(v);
            v.addNode(u);
        }

        return partitioning;
    }

    // compute mst with multiple machines
    public static Graph parallelMST(Graph graph, int machines, double eps, boolean showIntermediates) {
        // copy of input graph using the partition method
        Graph mst = graph.copy();

        // compute partial msts while we have too many edges
        int round = 0;
        while (mst.edges.size() > Math.pow(graph.nodes.size(), 1 + eps)) {
            // create machines partitions of the graph
            ArrayList<Graph> partitioning = partition(mst, machines);

            // compute partial msts
            ArrayList<Graph> msts = new ArrayList<>();

            // setup concurrent computation
            ExecutorService executor = Executors.newFixedThreadPool(machines);
            CompletionService<Graph> completionService = new ExecutorCompletionService<>(executor);

            // compute msts concurrently
            for (Graph part : partitioning) {
                // call disconnected prim on parts
                completionService.submit(() -> MSTBuilder.disconnectedPrim(part));
            }
            executor.shutdown();

            // get concurrent results
            while (msts.size() < machines) {
                try {
                    Future<Graph> futureMST = completionService.take();
                    msts.add(futureMST.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    return new Graph();
                }
            }

            // combine msts and generate adjacency through partition method
            mst = Graph.combineGraphs(msts);

            // showing intermediate graphs
            if (showIntermediates) {
                Driver.write(mst, "intermediate\\out_int" + round, 0);
                System.out.println(mst.edges.size());
            }
            round++;
        }

        return MSTBuilder.prim(mst.copy());
    }
}
