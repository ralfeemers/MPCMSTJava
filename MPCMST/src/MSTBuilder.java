import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

public class MSTBuilder {
    // depth first search
    private static void dfs(Graph sub, Graph graph, int n) {
        if (!sub.nodes.containsKey(n)) {
            sub.addNode(graph.nodes.get(n));
            for (int m : graph.nodes.get(n).adjacency.keySet()) {
                dfs(sub, graph, m);
            }
        }
    }

    // calculate disconnected graphs in toSub
    public static ArrayList<Graph> getSubGraphs(Graph toSub) {
        // init vars
        ArrayList<Graph> subGraphs = new ArrayList<>();
        int start = -1;
        HashMap<Integer, Boolean> visited = new HashMap<>();
        for (Node n : toSub.nodes.values()) {
            if (start == -1) {
                start = n.id;
                visited.put(start, true);
            } else {
                visited.put(n.id, false);
            }
        }

        // build sub graphs
        while (start != -1) {
            // calculate sub graph
            Graph sub = new Graph();
            dfs(sub, toSub, start);
            sub.calcEdges();
            subGraphs.add(sub);

            // set visited nodes
            for (int node : sub.nodes.keySet()) {
                visited.put(node, true);
            }

            // find non visited node
            start = -1;
            for (int node : visited.keySet()) {
                if (!visited.get(node)) {
                    start = node;
                    break;
                }
            }
        }

        return subGraphs;
    }

    // calculate mst edges with prim's algorithm on a connected graph using priority queue
    public static Graph prim(Graph graph) {
        // the mst
        Graph mst = new Graph(graph.nodes);

        // keep track of visited nodes
        int startNode = -1;
        HashMap<Integer, Boolean> visited = new HashMap<>();
        for (Node u : graph.nodes.values()) {
            if (startNode == -1) {
                startNode = u.id;
            }
            u.reset();
            visited.put(u.id, false);
        }

        // set starting node
        visited.put(startNode, true);
        graph.nodes.get(startNode).setDist(0);

        // init min-heap comparing nodes on distance from mst
        PriorityQueue<Node> heap = new PriorityQueue<>((u, v) -> {
            if (u.dist < v.dist) {
                return -1;
            } else if (u.dist > v.dist) {
                return 1;
            }
            return 0;
        });
        heap.addAll(graph.nodes.values());

        // initial node
        Node current = heap.remove();

        // while we don't have all edges
        while (mst.edges.size() < graph.nodes.size() - 1) {
            // set distances of nodes adjacent to current
            for (Node u : current.adjacency.values()) {
                // if node not in MST
                if (!visited.get(u.id)) {
                    // if node is closer to current node, then update dist in heap
                    double dist = Graph.dist(current, u);
                    if (dist < u.dist) {
                        heap.remove(u);
                        u.setNearest(current);
                        u.setDist(Graph.dist(current, u));
                        heap.add(u);
                    }
                }
            }

            // clear adjacent nodes of current node
            current.clearAdjacent();
            // pop nearest node
            current = heap.remove();
            // set visited
            visited.put(current.id, true);
            // add edge to mst
            current.addNode(current.nearest);
            current.nearest.addNode(current);
            mst.addEdge(new Edge(current.id, current.nearest.id, Graph.dist(current, current.nearest)));
        }

        // copy creates adjacency in mst
        return mst.copy();
    }

    // disconnected prim's algorithm
    public static Graph disconnectedPrim(Graph graph) {
        // calculate mst for each sub graph
        ArrayList<Graph> subGraphs = getSubGraphs(graph);
        for (Graph g : subGraphs) {
            Graph mst = prim(g);
            g.setNodes(mst.nodes);
            g.setEdges(mst.edges);
        }

        // combine sub graphs into single graph
        return Graph.combineGraphs(subGraphs);
    }

    // remove k-1 largest edges
    public static ArrayList<Graph> cluster(Graph graph, int k) {
        // find k-1 largest edges and remove
        for (int i = 0; i < k - 1; i++) {
            // find edge to remove
            double maxDist = Double.MIN_VALUE;
            Edge remove = new Edge(-1, -1, Double.MIN_VALUE);
            for (Edge e : graph.edges) {
                if (e.w > maxDist) {
                    remove = e;
                    maxDist = e.w;
                }
            }

            // remove edge
            graph.nodes.get(remove.u).adjacency.remove(remove.v);
            graph.nodes.get(remove.v).adjacency.remove(remove.u);
            graph.edges.remove(remove);
        }

        return getSubGraphs(graph);
    }
}
