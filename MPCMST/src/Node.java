import java.util.HashMap;

public class Node {
    int id;
    double dist;
    double x;
    double y;
    HashMap<Integer, Node> adjacency;
    Node nearest;

    public Node(int id, double x, double y) {
        this.id = id;
        this.dist = Double.MAX_VALUE;
        this.x = x;
        this.y = y;
        this.adjacency = new HashMap<>();
        nearest = null;
    }

    public void addNode(Node n) {
        adjacency.put(n.id, n);
    }

    public void clearAdjacent() {
        adjacency.clear();
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public void setNearest(Node n) {
        nearest = n;
    }

    public void reset() {
        dist = Double.MAX_VALUE;
    }

    public Node copy() {
        return new Node(id, x, y);
    }

    @Override
    public String toString() {
        return id + ":(" + x + ", " + y + ")";  // graph drawing output
//        return id + ":(" + dist + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Node)) {
            return false;
        }

        Node n = (Node) o;

        return n.id == this.id && n.x == this.x && n.y == this.y;
    }
}
